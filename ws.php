<?php

header("Access-Control-Allow-Origin: *");

function check_name($filename,$dir){
    $loc = $dir.$filename;

    if(file_exists($loc)){
        $increment = 0;
        list($name, $ext) = explode('.', $filename);

        while(file_exists($loc)) {
            $increment++;
            $loc = $dir.$name. $increment . '.' . $ext;
            $filename = $name. $increment . '.' . $ext;
        }
    }
    return $filename;
}

if (isset($_GET['save'])) {

    $url = $_GET['save'];
    $file_name = basename($url);
    $path = "docs/";     
    $file_name = check_name( $file_name,$path);
  
    if (file_put_contents("docs/".$file_name, file_get_contents($url))){
        echo $file_name;
    }else {
        printf("error method");
    }
}

?>