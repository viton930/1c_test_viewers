<!DOCTYPE html>
<html>

<head>
  <title>OfficeJs | Demos </title>
  <meta charset="utf-8">
  <!-- <link href="./layout/styles/layout.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" href="./include/jquery_ui/themes/start/jquery-ui.min.css">
  <script src="./include/jquery/jquery-1.12.4.min.js"></script>
  <script src="./include/jquery_ui/jquery-ui.min.js"></script>
  <!-- ################################ For files reder ###############################-->
  <!--PDF-->
  <link rel="stylesheet" href="./include/pdf/pdf.viewer.css">
  <script src="./include/pdf/pdf.js"></script>
  <!--Docs-->
  <script src="./include/docx/jszip-utils.js"></script>
  <script src="./include/docx/mammoth.browser.min.js"></script>
  <!--PPTX-->
  <link rel="stylesheet" href="./include/PPTXjs/css/pptxjs.css">
  <link rel="stylesheet" href="./include/PPTXjs/css/nv.d3.min.css">
  <link rel="stylesheet" href="./include/revealjs/reveal.css">

  <script type="text/javascript" src="./include/PPTXjs/js/filereader.js"></script>
  <script type="text/javascript" src="./include/PPTXjs/js/d3.min.js"></script>
  <script type="text/javascript" src="./include/PPTXjs/js/nv.d3.min.js"></script>
  <script type="text/javascript" src="./include/PPTXjs/js/pptxjs.js"></script>
  <script type="text/javascript" src="./include/PPTXjs/js/divs2slides.js"></script>
  <!--All Spreadsheet -->
  <link rel="stylesheet" href="./include/SheetJS/handsontable.full.min.css">
  <script type="text/javascript" src="./include/SheetJS/handsontable.full.min.js"></script>
  <script type="text/javascript" src="./include/SheetJS/xlsx.full.min.js"></script>
  <!--Image viewer-->
  <link rel="stylesheet" href="./include/verySimpleImageViewer/css/jquery.verySimpleImageViewer.css">
  <script type="text/javascript" src="./include/verySimpleImageViewer/js/jquery.verySimpleImageViewer.js"></script>
  <!--officeToHtml-->
  <script src="./officeToHtml.js"></script>
  <!-- <link rel="stylesheet" href="./officeToHtml.css"> -->
  <style>
    body,
    html {
      width: 100%;
      height: 100%;
      margin: 0;
      overflow: hidden;
    }

    #resolte-contaniner {
      width: 100%;
      height: 100%;
      overflow: auto;
      user-select: none;
      padding: 0 10px;
    }

    @media print {body {display: none}}
  </style>
</head>

<body>
  <!-- <div class="wrapper row3"> -->
    <div id="resolte-contaniner"></div>
    <script>
      var file_path = '<?=$FILE?>';
      $("#resolte-contaniner").officeToHtml({
        url: file_path,
      });
    </script>

  <!-- </div> -->
</body>

</html>